package model

type Division struct {
	ID   int    `json:"id,omitempty"`
	Name string `json:"name,omitempty"`
	Size int    `json:"size,omitempty"`
}
