package model

type Schedule struct {
	Weeks []week `json:"weeks,omitempty"`
}

type week struct {
	ID       int       `json:"id,omitempty"`
	Matchups []matchup `json:"matchup,omitempty"`
}

type matchup struct {
	ID     int                `json:"id,omitempty"`
	Teams  map[string]int     `json:"teams,omitempty"`    // Teams["home"] = 7, "home" and "away" as keys, values refer to Team IDs
	Score  map[string]float32 `json:"score,omitempty"`    // Score["home"] = 132.8, "home" and "away" as keys, values are the home and away team scores
	Winner int                `json:"position,omitempty"` // Reference Team ID
	Week   int                `json:"week,omitempty"`
}
