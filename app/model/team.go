package model

type Team struct {
	ID             int       `json:"id,omitempty"`
	Abbrev         string    `json:"abbrev,omitempty"`
	Name           string    `json:"name,omitempty"`
	Location       string    `json:"location,omitempty"`
	Nickname       string    `json:"nickname,omitempty"`
	LogoURL        string    `json:"logoUrl,omitempty"`
	Division       int       `json:"division,omitempty"` // Reference Division ID
	Owner          int       `json:"owner,omitempty"`    // Reference Owner ID
	WaiverRank     int       `json:"waiverRank,omitempty"`
	Standing       *Standing `json:"standing,omitempty"`
	OverallRecord  *Record   `json:"overallRecord,omitempty"`
	DivisionRecord *Record   `json:"divisionRecord,omitempty"`
	Players        []int     `json:"players,omitempty"` // Reference player IDs
}

type Standing struct {
	Overall       int     `json:"overall,omitempty"`
	Division      int     `json:"division,omitempty"`
	PointsFor     float64 `json:"pointsFor,omitempty"`
	PointsAgainst float64 `json:"pointsAgainst,omitempty"`
}

type Record struct {
	Wins       int     `json:"wins,omitempty"`
	Losses     int     `json:"losses,omitempty"`
	Ties       int     `json:"ties,omitempty"`
	Percentage float64 `json:"percentage,omitempty"`
}
