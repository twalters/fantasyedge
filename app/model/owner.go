package model

type Owner struct {
	ID        int    `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	FirstName string `json:"firstName,omitempty"`
	LastName  string `json:"lastName,omitempty"`
	Username  string `json:"username,omitempty"`
	PhotoURL  string `json:"photoUrl,omitempty"`
	Team      int    `json:"team,omitempty"` // Reference Team ID
}