package model

type Player struct {
	ID       string `json:"id,omitempty"`
	ESPNID int `json:"espnId,omitempty"`
	Name     string `json:"name,omitempty"`
	Position string `json:"position,omitempty"`
	OverallECR int `json:"overallEcr,omitempty"`
	PostionECR int `json:"postionEcr,omitreply"`

}
