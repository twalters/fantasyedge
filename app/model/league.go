package model

type League struct {
	ID        int        `json:"id,omitempty"`
	Name      string     `json:"name,omitempty"`
	Size      int        `json:"size,omitempty"`
	Schedule  Schedule   `json:"schedule,omitempty"`
	Teams     []Team     `json:"teams,omitempty"`
	Divisions []Division `json:"divisions,omitempty"`
	Owners    []Owner    `json:"owners,omitempty"`
	Settings  Settings   `json:"settings,omitempty"`
}
