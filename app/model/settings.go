package model

type Settings struct {
	Roster   Roster   `json:"roster,omitempty"`
	FlexType []string `json:"flexType,omitempty"`
}

type Roster struct {
	QB    int `json:"qb,omitempty"`
	RB    int `json:"rb,omitempty"`
	WR    int `json:"wr,omitempty"`
	TE    int `json:"te,omitempty"`
	FLEX  int `json:"flex,omitempty"`
	DST   int `json:"dst,omitempty"`
	K     int `json:"k,omitempty"`
	Bench int `json:"bench,omitempty"`
}

type Scoring struct {
}
