package router

import (
	"encoding/json"
	"net/http"

	"fmt"

	"github.com/twalters/fantasyedge/app/shared/datasource/espn"
)

func GetTestLeague(w http.ResponseWriter, req *http.Request) {
	// rawLeagueData := espn.GetLeagueData(888433)
	// rawTeamData := espn.GetTeamData(888433)
	rawScheduleData := espn.GetScheduleData(888433)
	// league := espn.GenerateLeagueFromData(rawLeagueData, rawTeamData)

	// json.NewEncoder(w).Encode(league)
	json.NewEncoder(w).Encode(rawScheduleData)
}

func GetLeagueById(id int) {

}

func GetTeams() {

}

func GetTeamById(id int) {

}

func GetDivisions() {

}

func GetDivisionById(id int) {

}

func GetMatchups() {

}

func GetMatchupsByTeam() {

}

func GetMatchupById(id int) {

}

func init() {
	fmt.Println("Adding API routes")
	APIRouter.HandleFunc("/test", GetTestLeague).Methods("GET")
}
