package espn

import (
	"net/http"
	"time"

	"encoding/json"

	"github.com/twalters/fantasyedge/app/model"
	"github.com/twalters/fantasyedge/app/shared/util"
)

const (
	baseURL      string = "http://games.espn.com/ffl/api/v2/"
	leaguePath   string = "leagueSettings"
	teamPath     string = "teams"
	schedulePath string = "schedule"
	boxScorePath string = "boxscore"
	seasonID     int    = 2016
)

type LeagueResponse struct {
	League   League   `json:"leaguesettings"`
	Metadata Metadata `json:"metadata"`
}

type TeamResponse struct {
	Teams    []Team   `json:"teams"`
	Metadata Metadata `json:"metadata"`
}

type ScheduleResponse struct {
	Schedule Schedule `json:"leagueSchedule"`
}

type League struct {
	ID        int              `json:"id"`
	Name      string           `json:"name"`
	Size      int              `json:"size"`
	Teams     []Team           `json:"teams"`
	Divisions []model.Division `json:"divisions"`
	Owners    []model.Owner    `json:"leagueMembers"`
}

type Team struct {
	ID         int            `json:"teamId"`
	Abbrev     string         `json:"teamAbbrev"`
	NickName   string         `json:"teamNickname"`
	Location   string         `json:"teamLocation"`
	LogoURL    string         `json:"logoUrl"`
	Division   model.Division `json:"division"`
	Owner      model.Owner    `json:"Owner"`
	WaiverRank int            `json:"waiverRank"`
	Record     TeamRecord     `json:"record"`
}

type TeamRecord struct {
	DivisionStanding   int     `json:"divisionStanding"`
	OverallStanding    int     `json:"overallStanding"`
	DivisionLosses     int     `json:"divisionLosses"`
	DivisionWins       int     `json:"divisionWins"`
	OverallTies        int     `json:"overallTies"`
	DivisionTies       int     `json:"divisionTies"`
	OverallPercentage  float64 `json:"overallPercentage"`
	OverallWins        int     `json:"overallWins"`
	OverallLosses      int     `json:"overallLosses"`
	PointsAgainst      float64 `json:"pointsAgainst"`
	DivisionPercentage float64 `json:"divisionPercentage"`
	PointsFor          float64 `json:"pointsFor"`
}

type Player struct {
	GameStarterStatus       int64     `json:"gameStarterStatus"`
	SportsID                int       `json:"sportsId"`
	LastName                string    `json:"lastName"`
	HealthStatus            int       `json:"healthStatus"`
	Droppable               bool      `json:"droppable"`
	PercentStarted          float64   `json:"percentStarted"`
	PlayerRatingSeason      float64   `json:"playerRatingSeason"`
	PercentOwned            float64   `json:"percentOwned"`
	ProTeamID               int       `json:"proTeamId"`
	TickerID                int       `json:"tickerId"`
	PositionRank            int       `json:"positionRank"`
	LastNewsDate            time.Time `json:"lastNewsDate"`
	IsActive                bool      `json:"isActive"`
	IsIREligible            bool      `json:"isIREligible"`
	DraftRank               float64   `json:"draftRank"`
	LastVideoDate           time.Time `json:"lastVideoDate"`
	PlayerID                int       `json:"playerId"`
	PercentChange           float64   `json:"percentChange"`
	DefaultPositionID       int       `json:"defaultPositionId"`
	Value                   int       `json:"value"`
	UniverseID              int       `json:"universeId"`
	EligibleSlotCategoryIds []int     `json:"eligibleSlotCategoryIds"`
	FirstName               string    `json:"firstName"`
	TotalPoints             float64   `json:"totalPoints"`
}

type Schedule struct {
	ScheduleItems []struct {
		Matchups []Matchup `json:"matchups"`
		Week     int       `json:"matchupPeriodId"`
	} `json:"scheduleItems"`
	Metadata Metadata `json:"metadata"`
}

type Matchup struct {
	MatchupTypeID  int   `json:"matchupTypeId"`
	AwayTeamScores []int `json:"awayTeamScores"`
	AwayTeam       struct {
		WaiverRank int `json:"waiverRank"`
		Division   struct {
			DivisionName string `json:"divisionName"`
			DivisionID   int    `json:"divisionId"`
			Size         int    `json:"size"`
		} `json:"division"`
		TeamAbbrev   string `json:"teamAbbrev"`
		TeamNickname string `json:"teamNickname"`
		LogoURL      string `json:"logoUrl"`
		TeamLocation string `json:"teamLocation"`
		TeamID       int    `json:"teamId"`
		LogoType     string `json:"logoType"`
	} `json:"awayTeam"`
	AwayTeamAdjustment int   `json:"awayTeamAdjustment"`
	AwayTeamID         int   `json:"awayTeamId"`
	IsBye              bool  `json:"isBye"`
	HomeTeamID         int   `json:"homeTeamId"`
	HomeTeamAdjustment int   `json:"homeTeamAdjustment"`
	HomeTeamScores     []int `json:"homeTeamScores"`
	HomeTeamBonus      int   `json:"homeTeamBonus"`
	Outcome            int   `json:"outcome"`
	HomeTeam           struct {
		WaiverRank int `json:"waiverRank"`
		Division   struct {
			DivisionName string `json:"divisionName"`
			DivisionID   int    `json:"divisionId"`
			Size         int    `json:"size"`
		} `json:"division"`
		TeamAbbrev   string `json:"teamAbbrev"`
		TeamNickname string `json:"teamNickname"`
		LogoURL      string `json:"logoUrl"`
		TeamLocation string `json:"teamLocation"`
		TeamID       int    `json:"teamId"`
		LogoType     string `json:"logoType"`
	} `json:"homeTeam"`
}

type Metadata struct {
	LeagueID           string    `json:"leagueId"`
	Status             string    `json:"status"`
	DateModifiedLeague time.Time `json:"dateModifiedLeague"`
	DateModifiedUser   time.Time `json:"dateModifiedUser"`
	SeasonID           string    `json:"seasonId"`
	ServerDate         string    `json:"serverDate"`
}

func getLeagueData(leagueID int) *LeagueResponse {
	url := util.BuildURL(baseURL, leaguePath, map[string]int{
		"leagueId": leagueID,
		"seasonId": seasonID,
	})

	resp, err := http.Get(url)
	util.ProcessError(err)

	data := &LeagueResponse{}
	json.NewDecoder(resp.Body).Decode(data)

	return data
}

func getTeamData(leagueID int) *TeamResponse {
	url := util.BuildURL(baseURL, teamPath, map[string]int{
		"leagueId": leagueID,
		"seasonId": seasonID,
	})

	resp, err := http.Get(url)
	util.ProcessError(err)

	data := &TeamResponse{}
	json.NewDecoder(resp.Body).Decode(data)

	return data
}

func getScheduleData(leagueID int) *ScheduleResponse {
	url := util.BuildURL(baseURL, schedulePath, map[string]int{
		"leagueId": leagueID,
		"seasonId": seasonID,
	})

	resp, err := http.Get(url)
	util.ProcessError(err)

	data := &ScheduleResponse{}
	json.NewDecoder(resp.Body).Decode(data)

	return data
}

func GenerateLeagueFromSource(leagueID int) model.League {
	rawLeague := getLeagueData(leagueID).League
	rawTeams := getTeamData(leagueID).Teams
	rawSchedule := getScheduleData(leagueID).Schedule

	league := model.League{
		ID:        rawLeague.ID,
		Name:      rawLeague.Name,
		Size:      rawLeague.Size,
		Divisions: rawLeague.Divisions,
		Owners:    rawLeague.Owners,
	}

	for _, team := range rawTeams {
		league.Teams = append(league.Teams, convertTeamDataToModel(team))
	}

	for _, matchup := range rawSchedule {
		league.Matchups = append(league.Matchups, convertMatchupDataToModel(matchup))
	}

	return league
}

func convertTeamDataToModel(teamData Team) model.Team {
	return model.Team{
		ID:         teamData.ID,
		Abbrev:     teamData.Abbrev,
		Name:       teamData.Location + " " + teamData.NickName,
		Location:   teamData.Location,
		Nickname:   teamData.NickName,
		LogoURL:    teamData.LogoURL,
		Division:   teamData.Division.ID,
		Owner:      teamData.Owner.ID,
		WaiverRank: teamData.WaiverRank,
		Standing: &model.Standing{
			Overall:       teamData.Record.OverallStanding,
			Division:      teamData.Record.DivisionStanding,
			PointsFor:     teamData.Record.PointsFor,
			PointsAgainst: teamData.Record.PointsAgainst,
		},
		OverallRecord: &model.Record{
			Wins:       teamData.Record.OverallWins,
			Losses:     teamData.Record.OverallLosses,
			Ties:       teamData.Record.OverallTies,
			Percentage: teamData.Record.OverallPercentage,
		},
		DivisionRecord: &model.Record{
			Wins:       teamData.Record.DivisionWins,
			Losses:     teamData.Record.DivisionLosses,
			Ties:       teamData.Record.DivisionTies,
			Percentage: teamData.Record.DivisionPercentage,
		},
	}
}

func convertScheduleDataToModel(scheduleData Schedule) model.Schedule {
	return model.Schedule{}
}
