package util

import (
	"log"
	"net/url"
	"strconv"
)

func ProcessError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func BuildURL(base string, path string, query map[string]int) string {
	params := url.Values{}
	for key, val := range query {
		params.Add(key, strconv.Itoa(val))
	}
	return base + path + "?" + url.Values.Encode(params)
}
