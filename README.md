Fantasy Edge
============

A set of tools to help people have a successful fantasy football season.

Tools
-----

### For Team Owners

* Team Analysis
* Add/Drop Analysis
* Trade Analysis
* VORP weekly and ROS projections

### For Commissioners

* Weekly Recaps - winners, losers, best and worst scoring teams, upsets, close games
* Season Recaps - Top performances, worst performances, biggest best/worst point differential
* Power Rankings